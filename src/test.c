#define _DEFAULT_SOURCE
#define MALLOC_SIZE 1024

#include "test.h"

int free_heap(void *heap, size_t length) {
    return munmap(heap, size_from_capacity((block_capacity) {.bytes = length}).bytes);
}

void *start_test(size_t test_number) {
    printf("Test #%zu started...\n", test_number);

    void *heap = heap_init(capacity_from_size((block_size) {REGION_MIN_SIZE}).bytes);

    debug_heap(stdout, heap);

    return heap;
}

void end_test(size_t test_number, void *data_to_destroy, void *heap_to_destroy) {
    _free(data_to_destroy);

    debug_heap(stdout, heap_to_destroy);

    free_heap(heap_to_destroy, REGION_MIN_SIZE);


    printf("Test #%zu has been passed successfully.\n", test_number);
}

bool simple_memory_allocation_test() {

    void *heap = start_test(1);

    if (!heap) {
        fprintf(stderr, "Test #1 failed!\n");
        return false;
    }

    void *data = _malloc(MALLOC_SIZE);

    debug_heap(stdout, heap);

    if (!data) {
        fprintf(stderr, "Test #1 failed!\n");
        return false;
    }

    end_test(1, data, heap);
    return true;
}

bool free_one_block_from_many_allocated_test() {
    void *heap = start_test(2);

    if (!heap) {
        fprintf(stderr, "Test #2 failed!\n");
        return false;
    }

    void *data1 = _malloc(MALLOC_SIZE);
    void *data2 = _malloc(MALLOC_SIZE);

    debug_heap(stdout, heap);

    _free(data1);

    debug_heap(stdout, heap);

    if (!data1 || !data2) {
        fprintf(stderr, "Test #2 failed!\n");
        return false;
    }

    end_test(2, data2, heap);
    return true;
}

bool free_two_block_from_many_allocated_test() {
    void *heap = start_test(3);

    if (!heap) {
        fprintf(stderr, "Test #3 failed!\n");
        return false;
    }

    void *data1 = _malloc(MALLOC_SIZE);
    void *data2 = _malloc(MALLOC_SIZE);
    void *data3 = _malloc(MALLOC_SIZE);

    debug_heap(stdout, heap);

    if (!data1 || !data2 || !data3) {
        fprintf(stderr, "Test #3 failed!\n");
        return false;
    }

    _free(data2);
    _free(data1);

    debug_heap(stdout, heap);

    if (!data3) {
        fprintf(stderr, "Test #3 failed!\n");
        return false;
    }

    end_test(3, data3, heap);
    return true;
}

bool grow_heap_test() {
    void *heap = start_test(4);

    if (!heap) {
        fprintf(stderr, "Test #4 failed!\n");
        return false;
    }

    void *data = _malloc(REGION_MIN_SIZE * 2);
    debug_heap(stdout, heap);

    struct block_header const *header = heap;
    if (data != HEAP_START + offsetof(struct block_header, contents)
            || header->capacity.bytes <= REGION_MIN_SIZE) {
        fprintf(stderr, "Test #4 failed!\n");
        return false;
    }

    end_test(4, data, heap);

    return true;
}

bool grow_heap_in_new_place_test() {
    void *heap = start_test(5);

    (void) mmap(heap + REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);

    void *data = _malloc(MALLOC_SIZE * 10);

    if (!data) {
        fprintf(stderr, "Test #4 failed!\n");
        return false;
    }

    end_test(5, data, heap);

    return true;
}
