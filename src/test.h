#ifndef ASSIGNMENT_4_MEMORY_ALLOCATOR_TEST_H
#define ASSIGNMENT_4_MEMORY_ALLOCATOR_TEST_H

#define TEST_COUNT 5

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <sys/mman.h>

bool simple_memory_allocation_test();

bool free_one_block_from_many_allocated_test();

bool free_two_block_from_many_allocated_test();

bool grow_heap_test();

bool grow_heap_in_new_place_test();

#endif
