#include "test.h"

int main() {
    return !(simple_memory_allocation_test()
             && free_one_block_from_many_allocated_test()
             && free_two_block_from_many_allocated_test()
             && grow_heap_test()
             && grow_heap_in_new_place_test());
}
